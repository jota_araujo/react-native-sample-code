import {createAppContainer} from 'react-navigation';
import {TransitionPresets} from 'react-navigation-stack';
import {createSharedElementStackNavigator} from 'react-navigation-shared-element';
//import UserLocationPage from './modules/location/UserLocationPage';
//import AddressFormPage from './modules/location/AddressFormPage';
//import HomeScreen from './modules/nubank/HomeScreen';
//import HistoryScreen from './modules/nubank/HistoryScreen';
import {TransitionPreset} from 'react-navigation-stack/lib/typescript/src/vendor/types';
import MainScreen from './modules/scratch/MainScreen';

if (__DEV__) {
  import('./ReactotronConfig');
}

//enableScreens();

const obj: TransitionPreset = Object.assign(
  {},
  TransitionPresets.FadeFromBottomAndroid,
);
// obj.transitionSpec.open.config.duration = 3000;
// obj.transitionSpec.close.config.duration = 2000;

const AppNavigator = createSharedElementStackNavigator(
  {
    Home: {
      screen: MainScreen,
    },
    /*     History: {
      screen: HistoryScreen,
    }, */
  },
  {
    initialRouteName: 'Home',
    headerMode: 'none',
    defaultNavigationOptions: {
      ...obj,
    },
  },
);

// @ts-ignore
export default createAppContainer(AppNavigator);
