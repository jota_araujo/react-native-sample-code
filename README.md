# Project 1: Nubank UI clone

Screen transition implemented using [react-navigation-shared-screen-element](https://github.com/IjzerenHein/react-navigation-shared-element) and the React Animated API.

![Nubank Home Screen UI](https://bitbucket.org/jota_araujo/react-native-sample-code/raw/fd193d7a5ab209c8c76b3c93bb9293e4204f5acd/modules/nubank/Screenshot-nubank-1.jpg) ![Nubank Purchase History UI](https://bitbucket.org/jota_araujo/react-native-sample-code/raw/fd193d7a5ab209c8c76b3c93bb9293e4204f5acd/modules/nubank/Screenshot-nubank-2.jpg)


# Project 2: Address Search and Map 

- User address auto-fill by zip code ( [ViaCEP](https://viacep.com.br/) webservice integration)
- Confirm user location on a Map (Google Maps API integration)

![Zip Code Search](https://bitbucket.org/jota_araujo/react-native-sample-code/raw/e1219ea7a5b078b60d2646e209c76668ce9c187c/Screenshot_1.jpg) ![Zip Code Search Results](https://bitbucket.org/jota_araujo/react-native-sample-code/raw/e1219ea7a5b078b60d2646e209c76668ce9c187c/Screenshot_2.jpg) ![Confirm location on Map](https://bitbucket.org/jota_araujo/react-native-sample-code/raw/e1219ea7a5b078b60d2646e209c76668ce9c187c/Screenshot_3.jpg)


# Project 3: Scratch Ticket (Raspadinha)

A scratch ticket implementation using SVG shapes and the react native PanResponder API. 


![Scratch Ticket](https://bitbucket.org/jota_araujo/react-native-sample-code/raw/8173bbfb4b1d6ab29ff2278d546b4001d04d5817/raspadinha2.gif)
