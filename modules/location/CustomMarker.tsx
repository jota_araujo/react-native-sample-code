import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import Svg, {Image} from 'react-native-svg';
import {Marker} from 'react-native-maps';

const CustomMarker = () => {
  const marker = {
    title: 'Title',
    description: 'Hello',
  };

  return (
    <Marker
      coordinate={{
        latitude: 37.78825,
        longitude: -122.4324,
      }}>
      <View style={styles.markerBackground}>
        <Svg width={40} height={30}>
          <Image
            href={{
              uri: 'https://randomuser.me/api/portraits/women/80.jpg',
            }}
            width={40}
            height={30}
          />
        </Svg>
        <View style={styles.textContainer}>
          <Text style={styles.markerTitle}>{marker.title}</Text>
          <Text style={styles.markerText}>{marker.description}</Text>
        </View>
      </View>
    </Marker>
  );
};

const styles = StyleSheet.create({
  markerBackground: {
    flexDirection: 'row',
    width: 100,
    height: 30,
    backgroundColor: 'orange',
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
    borderTopRightRadius: 15,
    borderTopLeftRadius: 15,
  },
  textContainer: {
    flexDirection: 'column',
  },
  markerTitle: {
    marginLeft: 2,
    fontSize: 9,
    color: '#ffffff',
    fontWeight: 'bold',
    textDecorationLine: 'underline',
  },
  markerText: {
    marginLeft: 2,
    fontSize: 9,
    color: '#ffffff',
    fontWeight: 'bold',
    textDecorationLine: 'underline',
  },
});

export default CustomMarker;
