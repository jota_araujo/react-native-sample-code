import React from 'react';
import {SafeAreaView, StyleSheet, View, TextInput, Text} from 'react-native';
import {useForm, Controller, FieldValues} from 'react-hook-form';
import Button from 'react-native-button';
import useViaCep from './use-via-cep';
import AddressForm, {styles as formStyles} from './AddressForm';

const notEmpty = (obj: object) => !!Object.keys(obj).length;

const AddressFormPage = () => {
  const {setCep, loading, error, data} = useViaCep();
  const {control, handleSubmit, formState} = useForm({
    mode: 'onChange',
  });
  const {dirty, isValid} = formState;
  const onSubmit = (values: FieldValues) => {
    setCep(values.cep);
  };

  return (
    <SafeAreaView style={styles.screen}>
      <View style={styles.buttonContainer}>
        <Text>Digite seu CEP:</Text>
        <Controller
          as={<TextInput style={formStyles.textInput} />}
          control={control}
          name="cep"
          onChange={args => args[0].nativeEvent.text}
          rules={{
            required: true,
            minLength: 8,
            maxLength: 8,
            pattern: /^[0-9]*$/,
          }}
          defaultValue=""
        />
        <Button
          style={formStyles.button}
          styleDisabled={[formStyles.button, formStyles.disabledButton]}
          containerStyle={{}}
          disabled={!dirty || !isValid}
          onPress={handleSubmit(onSubmit)}>
          {notEmpty(data) ? 'Pesquisar Novamente' : 'Continuar'}
        </Button>
      </View>
      {loading && <Text>{'Pesquisando CEP. Aguarde...'}</Text>}
      {!!error && <Text>{error.message}</Text>}
      {notEmpty(data) && <AddressForm address={data} />}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  screen: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'white',
    padding: 16,
  },
  buttonContainer: {
    paddingTop: 32,
    paddingBottom: 32,
  },
});

export default AddressFormPage;
