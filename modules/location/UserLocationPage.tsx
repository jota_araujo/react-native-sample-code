import React, {useReducer} from 'react';
import {SafeAreaView, StyleSheet, View, Button, ViewStyle} from 'react-native';
import {SvgXml} from 'react-native-svg';
import MapView, {PROVIDER_GOOGLE, Region} from 'react-native-maps';

const pin_xml = `
<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="#FF0000" d="M12,2C7.589,2,4,5.589,4,9.995C3.971,16.44,11.696,21.784,12,22c0,0,8.029-5.56,8-12C20,5.589,16.411,2,12,2z M12,14 c-2.21,0-4-1.79-4-4s1.79-4,4-4s4,1.79,4,4S14.21,14,12,14z"/></svg>`;

const pin_shadow = pin_xml.replace(/#FF0000/g, '#00000045');

type PinMarkerProps = {
  isDragging: boolean;
};

const PinMarker = ({isDragging}: PinMarkerProps) => {
  const pinStyle: ViewStyle = {
    position: 'absolute',
    left: isDragging ? -2 : 0,
    top: isDragging ? -6 : 0,
  };

  return (
    <View pointerEvents="none" style={[styles.pin]}>
      <SvgXml width="100%" height="100%" xml={pin_shadow} />
      <SvgXml style={pinStyle} width="100%" height="100%" xml={pin_xml} />
    </View>
  );
};

type ScreenState = {
  region: Region;
  isDragging: boolean;
};

const INITIAL_STATE: ScreenState = {
  region: {
    latitude: 37.78825,
    longitude: -122.4324,
    latitudeDelta: 0.015,
    longitudeDelta: 0.0121,
  },
  isDragging: false,
};

type Action = {
  type: 'REGION_UPDATE' | 'DRAG_START' | 'DRAG_END';
  region?: Region;
};

function reducer(state: ScreenState, action: Action): ScreenState {
  switch (action.type) {
    case 'REGION_UPDATE':
      return {
        ...state,
        region: action.region || state.region,
        isDragging: false,
      };
    case 'DRAG_START':
      return {
        ...state,
        isDragging: true,
      };

    default:
      return state;
  }
}

const UserLocationPage = () => {
  const [state, dispatch] = useReducer(reducer, INITIAL_STATE);

  return (
    <SafeAreaView style={styles.screen}>
      <View style={styles.mapContainer}>
        <MapView
          onRegionChangeComplete={(_region: Region) => {
            dispatch({type: 'REGION_UPDATE', region: _region});
          }}
          onPanDrag={() => {
            if (!state.isDragging) {
              dispatch({type: 'DRAG_START'});
            }
          }}
          minZoomLevel={9}
          provider={PROVIDER_GOOGLE} // remove if not using Google Maps
          style={styles.map}
          region={state.region}
        />
        <PinMarker isDragging={state.isDragging} />
      </View>

      <View style={styles.buttonContainer}>
        <Button onPress={() => {}} title={'Confirm Location'} />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  pin: {
    width: 50,
    height: 50,
  },
  screen: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'white',
  },
  mapContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  buttonContainer: {
    padding: 16,
    paddingTop: 16,
    paddingBottom: 32,
  },
});

export default UserLocationPage;
