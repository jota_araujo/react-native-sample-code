export type EmptyValue = {};

export type Address = {
  cep: string;
  logradouro?: string;
  complemento?: string;
  bairro?: string;
  localidade: string;
  uf: string;
  unidade?: string;
  ibge: string;
  gia?: string;
};

export type Error = {
  response: Response;
  message: string;
};

export type UseViaCepReturn = {
  cep: string;
  setCep: (cep: string) => void;
  loading: boolean;
  error: Error | null;
  data: Address | EmptyValue;
};
