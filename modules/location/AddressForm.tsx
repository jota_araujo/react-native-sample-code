import React from 'react';
import {StyleSheet, View, TextInput} from 'react-native';
import {useForm, Controller, Control, EmptyObject} from 'react-hook-form';
import Button from 'react-native-button';
import {Address} from './types';

type FormFieldProps = {
  control: Control;
  name: string;
  placeholderText: string;
  editable?: boolean;
};

const FormField = ({
  control,
  name,
  placeholderText,
  editable = true,
}: FormFieldProps) => {
  const opacity = editable ? 1 : 0.2;

  return (
    <Controller
      as={
        <TextInput editable={editable} style={[styles.textInput, {opacity}]} />
      }
      control={control}
      placeholder={placeholderText}
      name={name}
      onChange={args => args[0].nativeEvent.text}
      rules={{
        required: true,
      }}
    />
  );
};

type AddressFormProps = {
  address: Address | EmptyObject;
};

const safeStr = (obj: Address | EmptyObject, prop: keyof Address): string => {
  if (obj && prop) {
    return obj[prop] || '';
  }
  return '';
};

const AddressForm = ({address}: AddressFormProps) => {
  const {control, handleSubmit, reset, formState} = useForm<{
    logradouro: string;
    complemento: string;
    bairro: string;
    localidade: string;
    uf: string;
  }>({
    mode: 'onChange',
  });

  React.useEffect(() => {
    if (address !== {}) {
      reset({
        logradouro: safeStr(address, 'logradouro'),
        complemento: safeStr(address, 'complemento'),
        bairro: safeStr(address, 'bairro'),
        localidade: safeStr(address, 'localidade'),
        uf: safeStr(address, 'uf'),
      });
    }
  }, [address, reset]);

  const onSubmit = () => {
    //TODO: Navigate to Map page
  };

  const {dirty, isValid} = formState;
  return (
    <View style={styles.formContainer}>
      <FormField
        name="logradouro"
        control={control}
        placeholderText={'Logradouro'}
      />
      <FormField
        name="complemento"
        control={control}
        placeholderText={'Complemento'}
      />
      <FormField name="bairro" control={control} placeholderText={'Bairro'} />
      <FormField
        name="localidade"
        control={control}
        placeholderText={'Localidade'}
      />
      <FormField name="uf" control={control} placeholderText={'Estado'} />
      <Button
        style={styles.button}
        containerStyle={{}}
        disabled={!dirty || !isValid}
        onPress={handleSubmit(onSubmit)}>
        Próximo passo
      </Button>
    </View>
  );
};

export const styles = StyleSheet.create({
  formContainer: {
    flex: 1,
  },
  button: {
    marginBottom: 6,
    marginTop: 6,
    backgroundColor: '#24A0ED',
    color: 'white',
    fontSize: 18,
    padding: 16,
  },
  disabledButton: {
    backgroundColor: '#cccccc',
    color: '#eeeeee',
  },
  textInput: {
    borderWidth: StyleSheet.hairlineWidth,
    marginBottom: 6,
    marginTop: 6,
    paddingLeft: 8,
  },
});

export default AddressForm;
