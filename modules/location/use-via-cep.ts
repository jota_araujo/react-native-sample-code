// adapted from: https://github.com/lucasfloriani/use-via-cep/
import * as React from 'react';
import {Address, EmptyValue, Error, UseViaCepReturn} from './types';

type ParsedData = {
  erro?: boolean;
};

export default (initialCep = ''): UseViaCepReturn => {
  const [cep, setCep] = React.useState<string>(initialCep);
  const [loading, setLoading] = React.useState<boolean>(false);
  const [error, setError] = React.useState<Error | null>(null);
  const [data, setData] = React.useState<Address | EmptyValue>({});

  React.useEffect(() => {
    const regexedCep = cep.replace(/\D/g, '');
    if (regexedCep.length !== 8) {
      setData({});
      return;
    }

    setLoading(true);
    setError(null);

    fetch(`https://viacep.com.br/ws/${regexedCep}/json`)
      .then(async (resp: Response) => {
        setLoading(false);
        if (!resp.ok) {
          setError({response: resp, message: 'Erro ao carregar os dados'});
          return;
        }
        const parsed = (await resp.json()) as ParsedData;
        if (parsed.erro) {
          setError({
            response: resp,
            message: 'CEP inválido ou não encontrado.',
          });
          return;
        }
        return parsed;
      })
      .then(_data => {
        // console.log(_data);
        if (_data && !_data.erro) {
          setData((_data as unknown) as Address);
        }
      });
  }, [cep]);

  return {cep, setCep, loading, error, data};
};
