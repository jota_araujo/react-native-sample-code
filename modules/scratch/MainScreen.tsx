import React, {useState} from 'react';
// @ts-ignore
import throttle from 'lodash.throttle';
import {PanResponder, View, Image, StyleSheet, Dimensions} from 'react-native';
import MaskedView from '@react-native-community/masked-view';
import MaskElement, {Point} from './components/MaskElement';
import dataPoints from './data';
import {screenWidth} from './constants';

type MaskProps = {
  points: Point[];
};

const Mask = ({points}: MaskProps) => {
  return (
    <View style={styles.mask}>
      {points.map((item, index) => {
        return item.show > 0 ? (
          <MaskElement
            key={`${item.x}-${item.y}`}
            index={index}
            show={item.show}
            point={item}
          />
        ) : (
          false
        );
      })}
    </View>
  );
};

const erase = throttle((x: number, y: number, setDrawPoints: Function) => {
  const x0 = Math.floor(x / (screenWidth / 12));
  const y0 = Math.floor((y - screenWidth * 0.5) / 30);
  setDrawPoints((prevState: Point[]) => {
    return [
      ...prevState.map((el: Point) => {
        if (el.x === x0 && el.y === y0) {
          const inc = Math.random() > 0.7 ? 1 : 2;
          return Object.assign({}, el, {show: Math.min(el.show + inc, 2)});
        }
        return el;
      }),
    ];
  });
}, 60);

const MainScreen = () => {
  const [drawPoints, setDrawPoints] = useState<Point[]>(dataPoints);

  const panResponder = React.useRef(
    PanResponder.create({
      // Ask to be the responder:
      onStartShouldSetPanResponder: () => {
        return true;
      },
      onPanResponderGrant: (evt, gestureState) => {
        const {x0, y0} = gestureState;
        erase(x0, y0, setDrawPoints);
      },
      onPanResponderMove: (evt, gestureState) => {
        const {moveX, moveY} = gestureState;
        erase(moveX, moveY, setDrawPoints);
      },
      onStartShouldSetPanResponderCapture: () => true,
      onMoveShouldSetPanResponder: () => true,
      onMoveShouldSetPanResponderCapture: () => true,
      onPanResponderTerminationRequest: () => true,
    }),
  ).current;

  return (
    <View style={styles.screen}>
      <View style={styles.ticket}>
        <MaskedView
          pointerEvents="none"
          style={styles.maskView}
          maskElement={<Mask points={drawPoints} />}>
          <Image
            source={require('./jackpot.jpg')}
            resizeMode="stretch"
            style={styles.image}
          />
        </MaskedView>
        <View {...panResponder.panHandlers} style={styles.overlay} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: '#1a444d',
  },
  ticket: {
    position: 'absolute',
    ...StyleSheet.absoluteFillObject,
    top: screenWidth * 0.5,
    maxHeight: 218,
    backgroundColor: '#e6dbbe',
  },
  maskView: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  mask: {
    top: 0,
    left: 0,
    width: '100%',
    position: 'absolute',
    backgroundColor: 'transparent',
    flex: 1,
  },
  image: {
    width: '100%',
    height: '100%',
  },
  overlay: {
    position: 'absolute',
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'transparent',
    flex: 1,
  },
});

export default MainScreen;
