export default new Array(7)
  .fill(1)
  .map((v, index) => index)
  .map(row =>
    new Array(12).fill(row).map((_row: number, index: number) => {
      return {
        show: 0,
        x: index,
        y: _row,
        offsetX: Math.floor(Math.random() * 3),
        offsetY: Math.floor(Math.random() * 3),
        shape: Math.floor(Math.random() * 3),
      };
    }),
  )
  .flat();
