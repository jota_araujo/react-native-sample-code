import React from 'react';
import {View} from 'react-native';
import SimpleLineIcon from 'react-native-vector-icons/SimpleLineIcons';
import FAIcon from 'react-native-vector-icons/FontAwesome';
import FeatherIcon from 'react-native-vector-icons/Feather';
import EvilIcon from 'react-native-vector-icons/EvilIcons';

export type IconType = {
  set: 'sl' | 'fa' | 'fe' | 'ev';
  name: string;
  size?: number;
  color?: string;
};

const Icon = ({set, name, size = 20, color = '#000'}: IconType) => {
  switch (set) {
    case 'fa':
      return <FAIcon name={name} size={size} color={color} />;
    case 'sl':
      return <SimpleLineIcon name={name} size={size} color={color} />;
    case 'fe':
      return <FeatherIcon name={name} size={size} color={color} />;
    case 'ev':
        return <EvilIcon name={name} size={size} color={color} />;
    default:
      return <View />;
  }
};

export default Icon;
