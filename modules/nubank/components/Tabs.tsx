import React from 'react';
import {StyleSheet, View, Text, ScrollView} from 'react-native';
import Icon, {IconType} from './Icon';
import {Size, Typography} from '../constants';

const DATA: TabData[] = [
  {
    label: 'Indicar amigos',
    icon: {
      set: 'sl',
      name: 'user-follow',
    },
  },
  {
    label: 'Recarga de celular',
    icon: {
      set: 'sl',
      name: 'screen-smartphone',
    },
  },
  {
    label: 'Ajustar limite',
    icon: {
      set: 'fa',
      name: 'sliders',
    },
  },
  {
    label: 'Me Ajuda',
    icon: {
      set: 'sl',
      name: 'question',
    },
  },
  {
    label: 'Bloquear cartão',
    icon: {
      set: 'fe',
      name: 'unlock',
    },
  },
];

type TabData = {
  label: string;
  icon: IconType;
};

type TabProps = {
  data: TabData;
};

const Tab = ({data}: TabProps) => {
  return (
    <View style={styles.tab}>
      <Icon {...data.icon} color="#FFF" />
      <Text style={styles.text}>{data.label}</Text>
    </View>
  );
};

const Tabs = () => {
  return (
    <ScrollView
      style={styles.scroller}
      horizontal
      showsHorizontalScrollIndicator={true}
      contentContainerStyle={styles.container}>
      {DATA.map((el, index) => {
        el.icon.size = Size.icon;
        return <Tab key={index} data={el} />;
      })}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  scroller: {
    position: 'absolute',
    bottom: 0,
  },
  container: {
    padding: Size.border,
    alignItems: 'center',
  },
  tab: {
    width: 90,
    height: 90,
    backgroundColor: '#FFFFFF25',
    borderRadius: 2,
    justifyContent: 'space-between',
    marginRight: Size.border / 2,
    padding: 8,
  },
  text: {
    color: 'white',
    ...Typography.tiny,
  },
});

export default Tabs;
