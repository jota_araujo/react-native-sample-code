import React from 'react';
import {StyleSheet, View, ScrollView, Platform} from 'react-native';
import {Size} from '../constants';
import CardStatus from './CardStatus';

const CARD_WIDTH = Size.screenWidth - Size.border * 2;
const CARD_HEIGHT = Size.screenHeight * 0.5;

const Cards = () => {
  return (
    <ScrollView
      horizontal
      snapToInterval={CARD_WIDTH + Size.border / 2} //your element width
      style={styles.cards}
      contentContainerStyle={styles.contentContainerStyle}>
      <View style={styles.cardContainer}>
        <CardStatus id="card1" />
      </View>
      <View style={[styles.cardContainer, styles.lastCard]}>
        <CardStatus id="card2" />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  cards: {
    flex: 1,
  },
  cardContainer: {
    marginRight: Size.border * 2,
    height: CARD_HEIGHT,
    minWidth: CARD_WIDTH,
  },
  contentContainerStyle: {
    paddingHorizontal: Platform.OS === 'android' ? Size.border : 0,
    alignItems: 'center',
  },
  lastCard: {marginRight: 0},
});

export default Cards;
