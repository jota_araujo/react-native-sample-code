import React, {useCallback, useState} from 'react';
import {StyleSheet, View, Text, Animated} from 'react-native';

import {Colors, Typography, Size} from '../constants';
import Icon from './Icon';
import PurchaseHistory from './PurchaseHistory';
import {CreditStatusBar} from './CreditStatusBar';
import {TouchableNativeFeedback} from 'react-native-gesture-handler';
import {SharedElement} from 'react-navigation-shared-element';

type CardHeaderProps = {
  opacity: Animated.Value;
};

export const CardHeader = ({opacity}: CardHeaderProps) => {
  return (
    <Animated.View style={[styles.header, {opacity}]}>
      <Icon set="sl" name="credit-card" color="#999" />
      <Text style={styles.headerText}>CARTÃO DE CRÉDITO</Text>
    </Animated.View>
  );
};

type FooterProps = {
  onPress: Function;
};

export const CardFooter = ({onPress}: FooterProps) => (
  <View style={styles.footer}>
    <TouchableNativeFeedback
      onPress={() => {
        onPress();
      }}
      style={styles.footerButton}>
      <LatestPurchase />
    </TouchableNativeFeedback>
  </View>
);

type SummaryProps = {
  opacity: Animated.Value;
};

const Summary = ({opacity}: SummaryProps) => {
  return (
    <Animated.View style={[styles.body, {opacity}]}>
      <Text style={styles.bodyTitle}>FATURA ATUAL</Text>
      <Text style={styles.bodyText}>
        R$ <Text style={Typography.bold}>1382</Text>,00
      </Text>
      <Text style={styles.limitText}>
        {'Limite disponivel: '}
        <Text style={styles.limitTextAmmount}>R$ 2618</Text>
      </Text>
    </Animated.View>
  );
};

const LatestPurchase = () => {
  return (
    <View style={styles.footerContent}>
      <Icon set="sl" name="cup" color="#999" />
      <Text style={styles.footerText}>
        {'Compra mais recente em Starbucks\n no valor de R$ 17,50.'}
      </Text>
    </View>
  );
};

type CardStatusProps = {
  id: string;
};

const CardStatus = ({id}: CardStatusProps) => {
  const opacity = new Animated.Value(1);
  const [showHistory, setShowHistory] = useState(false);

  const fadeOut = useCallback(() => {
    Animated.timing(opacity, {
      toValue: 0,
      duration: 300,
      delay: 100,
    }).start(() => setTimeout(() => setShowHistory(true), 50));
  }, [opacity]);

  return (
    <SharedElement id={id} style={styles.card}>
      <View style={styles.cardContent}>
        <View style={styles.leftContent}>
          {showHistory ? (
            <PurchaseHistory />
          ) : (
            <>
              <CardHeader opacity={opacity} />
              <Summary opacity={opacity} />
              <CardFooter onPress={fadeOut} />
            </>
          )}
        </View>
        <SharedElement id="bar" style={styles.rightContent}>
          <CreditStatusBar />
        </SharedElement>
      </View>
    </SharedElement>
  );
};

export default CardStatus;

const styles = StyleSheet.create({
  flex1: {
    flex: 1,
  },
  absolutePosition: {
    position: 'absolute',
  },
  card: {
    width: '100%',
    height: '100%',
    backgroundColor: 'white',
  },
  cardContent: {
    flex: 3,
    flexDirection: 'row',
    width: '100%',
  },
  leftContent: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  rightContent: {
    position: 'absolute',
    right: 0,
    height: '100%',
    width: 8,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: Size.border,
    flex: 1,
  },
  headerSeparator: {
    width: '100%',
    height: StyleSheet.hairlineWidth,
    backgroundColor: Colors.darkGray,
  },
  headerText: {
    color: 'gray',
    ...Typography.small,
    marginLeft: Size.border,
  },
  body: {
    flex: 2,
    justifyContent: 'center',
    backgroundColor: 'white',
    paddingLeft: Size.border,
  },
  bodyTitle: {
    ...Colors.text.teal,
    ...Typography.tiny,
    fontWeight: 'bold',
    marginTop: 20,
  },
  bodyText: {
    ...Colors.text.teal,
    ...Typography.h2,
  },
  limitText: {
    color: 'black',
    ...Typography.average,
  },
  limitTextAmmount: {
    ...Colors.text.lightGreen,
    ...Typography.bold,
  },
  footer: {
    flex: 1,
    ...Colors.background.ligthGray,
  },
  footerButton: {
    width: '100%',
    height: '100%',
  },
  footerContent: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: Size.border,
  },
  footerText: {
    ...Colors.text.darkGray,
    ...Typography.tiny,
    marginLeft: Size.border,
  },
});
