import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Colors} from '../constants';

export const CreditStatusBar = () => {
  return (
    <View style={styles.creditBar}>
      <View style={styles.creditUsed} />
      <View style={styles.creditBill} />
      <View style={styles.creditAvailable} />
    </View>
  );
};

const styles = StyleSheet.create({
  creditBar: {
    flex: 1,
    width: '100%',
  },
  creditUsed: {
    ...Colors.background.orange,
    flex: 1,
  },
  creditBill: {
    ...Colors.background.teal,
    flex: 3,
  },
  creditAvailable: {
    ...Colors.background.lightGreen,
    flex: 5,
  },
});
