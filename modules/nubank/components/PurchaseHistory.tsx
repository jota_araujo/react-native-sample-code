import React, {useEffect} from 'react';
import {useNavigation} from 'react-navigation-hooks';
import {StyleSheet, View, Text, Animated, FlatList} from 'react-native';
import {Colors, Typography, Size} from '../constants';
import Icon, {IconType} from './Icon';

type PurchaseRecord = {
  icon: IconType;
  category: string;
  place: string;
};

const data: PurchaseRecord[] = [
  {
    icon: {
      set: 'ev',
      name: 'cart',
    },
    category: 'Supermercado',
    place: 'Extra Brigadeiro',
  },
  {
    icon: {
      set: 'ev',
      name: 'heart',
    },
    category: 'Saúde',
    place: 'Drogaria São Paulo',
  },
  {
    icon: {
      set: 'sl',
      name: 'paper-clip',
    },
    category: 'Papelaria',
    place: 'Kalunga',
  },
  {
    icon: {
      set: 'sl',
      name: 'cup',
    },
    category: 'ALimentação',
    place: 'Burger King Scp',
  },
];

export const History = () => {
  return (
    <FlatList
      data={data.concat([...data, ...data])}
      keyExtractor={(item, index) => index.toString()}
      renderItem={({item, index}) => (
        <View style={styles.listItem}>
          <View style={styles.listItemLeft}>
            <View style={styles.upperLineSegment} />
            <Icon
              color={Colors.gray}
              set={item.icon.set}
              name={item.icon.name}
              size={item.icon.set === 'ev' ? 25 : 20}
            />
            <View style={styles.lowerLineSegment} />
          </View>
          <View style={styles.listItemMiddle}>
            <Text style={styles.categoryText}>{item.category}</Text>
            <Text style={styles.text}>{item.place}</Text>
            <Text style={styles.text}>{`R$ ${Math.round(Math.random() * 80) +
              10},50`}</Text>
          </View>
          <View style={styles.listItemRight}>
            <Text style={styles.dateText}>{index > 3 ? 'Sexta' : 'Ontem'}</Text>
          </View>
        </View>
      )}
    />
  );
};

const PurchaseHistory = () => {
  const {navigate} = useNavigation();
  const opacity = new Animated.Value(0);
  const flex = new Animated.Value(0.25);

  useEffect(() => {
    Animated.parallel([
      Animated.timing(opacity, {
        toValue: 1,
        duration: 100,
      }),
      Animated.timing(flex, {
        toValue: 1,
        duration: 200,
      }),
    ]).start(() => setTimeout(() => navigate('History', {}), 100));
  }, [flex, navigate, opacity]);

  return (
    <Animated.View style={{...Colors.background.ligthGray, flex, opacity}}>
      <History />
    </Animated.View>
  );
};

export default PurchaseHistory;

const styles = StyleSheet.create({
  flex1: {
    flex: 1,
  },
  absolutePosition: {
    position: 'absolute',
  },
  separator: {
    height: 1,
    width: '100%',
    backgroundColor: '#CED0CE',
  },
  listItem: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    height: 100,
  },
  listItemLeft: {
    width: '20%',
    alignItems: 'center',
  },
  listItemMiddle: {
    flex: 1,
    justifyContent: 'center',
  },
  listItemRight: {
    width: '20%',
    height: '70%',
    marginRight: Size.border * 2,
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
  },
  upperLineSegment: {
    ...Colors.background.gray,
    height: '25%',
    width: StyleSheet.hairlineWidth,
    marginBottom: 3,
  },
  lowerLineSegment: {
    flex: 1,
    ...Colors.background.gray,
    width: StyleSheet.hairlineWidth,
    marginTop: 3,
  },
  categoryText: {
    ...Typography.tiny,
    ...Typography.bold,
    ...Colors.text.darkGray,
  },
  dateText: {
    ...Typography.micro,
    ...Colors.text.gray,
  },
  text: {
    ...Typography.average,
    ...Colors.text.darkGray,
    marginTop: 2,
  },
});
