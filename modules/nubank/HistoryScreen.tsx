import React, {useEffect} from 'react';
import {StyleSheet, View, Animated, StatusBar} from 'react-native';
import SafeAreaView from 'react-native-safe-area-view';
import {Size, Colors} from './constants';
import {History as PurchaseHistoryList} from './components/PurchaseHistory';
//import {CardHeader} from './components/CardStatus';
import {SharedElement} from 'react-navigation-shared-element';
import {CreditStatusBar} from './components/CreditStatusBar';
import {CardHeader} from './components/CardStatus';
import changeNavBarColor from './util/change-nav-bar-color';

const History = () => {
  const padding = new Animated.Value(Size.border);
  const flex = new Animated.Value(0.5);

  useEffect(() => {
    changeNavBarColor('#FFFFFF', true);
    Animated.sequence([
      Animated.timing(padding, {
        toValue: 0,
        duration: 150,
        delay: 200,
      }),
      Animated.timing(flex, {
        toValue: 1,
        duration: 300,
      }),
    ]).start();
  }, [flex, padding]);

  return (
    <View style={styles.historyContainer}>
      <Animated.View
        style={[
          styles.history,
          {
            flex,
            paddingLeft: padding,
            paddingRight: padding,
          },
        ]}>
        <SharedElement style={styles.list} id="card1">
          <>
            <View style={styles.leftContent}>
              <View style={styles.header}>
                <CardHeader opacity={new Animated.Value(1)} />
              </View>
              <PurchaseHistoryList />
            </View>
            <View style={styles.rightContent}>
              <CreditStatusBar />
            </View>
          </>
        </SharedElement>
      </Animated.View>
    </View>
  );
};

const HistoryScreen = () => {
  return (
    <SafeAreaView style={styles.screen}>
      <StatusBar barStyle="dark-content" backgroundColor="#FFF" />
      <History />
    </SafeAreaView>
  );
};

HistoryScreen.sharedElements = () => {
  return ['card1'];
};

const styles = StyleSheet.create({
  screen: {
    ...StyleSheet.absoluteFillObject,
  },
  leftContent: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  rightContent: {
    position: 'absolute',
    right: 0,
    height: '100%',
    width: 10,
  },
  list: {
    ...Colors.background.ligthGray,
    width: '100%',
    height: '100%',
  },
  historyContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    ...Colors.background.purple,
  },
  history: {
    width: '100%',
    flex: 0.5,
    paddingRight: Size.border / 2,
    paddingLeft: Size.border / 2,
  },
  header: {
    width: '100%',
    marginTop: 25,
    height: 60,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: Colors.gray,
    backgroundColor: 'white',
  },
  graph: {
    position: 'absolute',
    top: 100,
    width: '100%',
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default HistoryScreen;
