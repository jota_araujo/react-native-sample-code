import {StyleSheet, Dimensions} from 'react-native';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

const colorCodes = {
  purple: '#81269d',
  orange: '#f9b215',
  teal: '#1fbcca',
  lightGreen: '#93cf47',
  ligthGray: '#f5f5f5',
  gray: '#999',
  darkGray: '#666',
};

const backgroundColors = {
  purple: {
    backgroundColor: colorCodes.purple,
  },
  orange: {
    backgroundColor: colorCodes.orange,
  },
  teal: {
    backgroundColor: colorCodes.teal,
  },
  lightGreen: {
    backgroundColor: colorCodes.lightGreen,
  },
  ligthGray: {
    backgroundColor: colorCodes.ligthGray,
  },
  darkGray: {
    backgroundColor: colorCodes.darkGray,
  },
  gray: {
    backgroundColor: colorCodes.gray,
  },
};

const textColors = {
  purple: {
    color: colorCodes.purple,
  },
  orange: {
    color: colorCodes.orange,
  },
  teal: {
    color: colorCodes.teal,
  },
  lightGreen: {
    color: colorCodes.lightGreen,
  },
  ligthGray: {
    color: colorCodes.ligthGray,
  },
  darkGray: {
    color: colorCodes.darkGray,
  },
  gray: {
    color: colorCodes.gray,
  },
};

export const Colors = {
  ...colorCodes,
  background: StyleSheet.create({
    ...backgroundColors,
  }),
  text: StyleSheet.create({
    ...textColors,
  }),
};

export const Typography = StyleSheet.create({
  micro: {fontSize: 10},
  tiny: {fontSize: 12},
  small: {fontSize: 14},
  average: {fontSize: 16},
  large: {fontSize: 20},
  h3: {fontSize: 24},
  h2: {fontSize: 28},
  h1: {fontSize: 32},
  bold: {fontWeight: 'bold'},
});

export const Size = {
  screenHeight,
  screenWidth,
  border: screenWidth * 0.05,
  icon: 20,
};
