import React, {useEffect} from 'react';
import {StyleSheet, View, Text, StatusBar} from 'react-native';
import {SvgXml} from 'react-native-svg';
import Icon from 'react-native-vector-icons/SimpleLineIcons';

import Tabs from './components/Tabs';
import Cards from './components/Cards';
import logoXml from './assets/nubank-logo';
import {Colors, Size, Typography} from './constants';
import changeNavBarColor from './util/change-nav-bar-color';

const HomeScreen = () => {
  useEffect(() => {
    changeNavBarColor(Colors.purple, false);
  }, []);

  return (
    <View style={styles.screen}>
      <StatusBar
        translucent
        backgroundColor={Colors.purple}
        barStyle="light-content"
      />
      <View style={styles.header}>
        <View style={styles.title}>
          <SvgXml fill="white" width={40} height={40} xml={logoXml} />
          <Text style={styles.titleText}>João</Text>
        </View>
        <Icon name="arrow-down" size={10} color="white" />
      </View>

      <Cards />
      <Tabs />
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    ...StyleSheet.absoluteFillObject,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.purple,
  },
  header: {
    position: 'absolute',
    top: Size.border * 2,
    alignItems: 'center',
    height: 140,
  },
  title: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: Size.border / 2,
  },
  titleText: {
    color: 'white',
    ...Typography.bold,
    ...Typography.large,
    paddingBottom: 3,
    marginLeft: 8,
  },
});

export default HomeScreen;
