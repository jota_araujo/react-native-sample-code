import changeNavigationBarColor from 'react-native-navigation-bar-color';

export default async (color: string, light: boolean) => {
  try {
    const response = await changeNavigationBarColor(color, light, true);
    console.log(response);
  } catch (e) {
    //
  }
};
