import Reactotron from 'reactotron-react-native';

export default class Logger {
  static log(...args: any[]) {
    if (Reactotron && Reactotron.log) {
      Reactotron.log(...args);
    }
  }
  static warn(message: any) {
    if (Reactotron && Reactotron.warn) {
      Reactotron.warn(message);
    }
  }
  static error(message: any, stack: any) {
    if (Reactotron && Reactotron.error) {
      Reactotron.error(message, stack);
    }
  }
}
